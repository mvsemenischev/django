from django import forms
from .models import Services


class ContactForm(forms.Form):
    name = forms.CharField(min_length=2,
                           widget=forms.TextInput(
                               attrs={'placeholder': 'имя',
                                      'class': "feedback_block_input_name",
                                      'required': 'required'
                                      }
                           )
                           )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'placeholder': 'e-mail',
                   'class': "feedback_block_input_email",
                   'required': 'required'
                   }
        )
    )
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder': 'Комментарий',
                   'class': "feedback_block_input_description",
                   'required': 'required'
                   }
        )
    )
    services = Services.objects.all()
    list_of_services = []
    for i in range(1, len(services) + 1):
        list_of_services.append((i, services[i - 1].name))
    service = forms.ChoiceField(
        choices=tuple(list_of_services),
    )
