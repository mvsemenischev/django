from django.db import models


# Create your models here.
class FirstClass(models.Model):
    num = models.IntegerField()


class Services(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=512)
    cost = models.IntegerField()


class Feedback(models.Model):
    name = models.CharField(max_length=128)
    email = models.EmailField()
    choise = models.CharField(max_length=128)
    description = models.CharField(max_length=512)
