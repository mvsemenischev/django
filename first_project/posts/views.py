from django.shortcuts import render

from .forms import ContactForm
from .models import FirstClass, Services, Feedback


def index(request):
    return render(request, 'index.html')


# def index2(request):
#     obj = FirstClass.objects.all()
#     return render(request, "index2.html", {'obj': obj})


def main_page(request):
    context = {}

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            feedback(form.cleaned_data['name'], form.cleaned_data['email'], form.cleaned_data['service'],
                     form.cleaned_data['message'])
            context['success'] = 1
    else:
        form = ContactForm()
    context['form'] = form
    service = Services.objects.all()
    context['obj'] = service
    return render(request, 'html/task1.html', context=context)


# Create your views here.

def feedback(name, email, choise, message):
    new_feedback = Feedback(name=name, email=email, choise=Services.objects.get(id=choise).name, description=message)
    new_feedback.save()
