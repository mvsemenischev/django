from django.contrib import admin
from .models import FirstClass, Services, Feedback

admin.site.register(FirstClass)
admin.site.register(Services)
admin.site.register(Feedback)
# Register your models here.
